package postfix;

import org.junit.Assert;
import org.junit.Test;

public class PostfixTest {

	private PostfixCalculator calculator = new PostfixCalculator();

	@Test
	public void oneOperandExpression() {
		Assert.assertEquals(0d, calculator.calc("0"), 0);
		Assert.assertEquals(10d, calculator.calc("10"), 0);
	}

	@Test
	public void twoOperandsExpression() {
		Assert.assertEquals(3d, calculator.calc("1,2+"), 0);
		Assert.assertEquals(1d, calculator.calc("2,1-"), 0);
		Assert.assertEquals(6d, calculator.calc("2,3*"), 0);
		Assert.assertEquals(2d, calculator.calc("6,3/"), 0);
	}

	@Test
	public void threeOperandsExpression() {
		Assert.assertEquals(6d, calculator.calc("1,2,3++"), 0);
	}

}
