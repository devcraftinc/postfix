package postfix;

import java.util.Stack;

public class StackCalculator {

	private Stack<Double> operands = new Stack<>();

	public double top() {
		return operands.peek();
	}

	public void push(double operand) {
		operands.push(operand);
	}

	public void apply(char operator) {
		if ('+' == operator)
			operands.push(operands.pop() + operands.pop());
		else if ('-' == operator)
			operands.push(-operands.pop() + operands.pop());
		else if ('*' == operator)
			operands.push(operands.pop() * operands.pop());
		else if ('/' == operator)
			operands.push(1d / operands.pop() * operands.pop());
	}

}
