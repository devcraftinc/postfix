package postfix;

public class PostfixCalculator {

	public double calc(String expression) {
		PostfixTokenizer tokenizer = new PostfixTokenizer(expression);
		StackCalculator stackCalculator = new StackCalculator();
		
		String token = tokenizer.nextToken();
		while (token != null) {
			if (isNumber(token)) {
				double operand = asNumber(token);
				stackCalculator.push(operand);
			} else {
				stackCalculator.apply(asOperator(token));
			}
			token = tokenizer.nextToken();
		}
		return stackCalculator.top();
	}

	private char asOperator(String token) {
		return token.charAt(0);
	}

	private boolean isNumber(String token) {
		return Character.isDigit(token.charAt(0));
	}

	private double asNumber(String token) {
		return Double.parseDouble(token);
	}

}