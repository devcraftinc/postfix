package postfix;

import static java.lang.Character.isDigit;

public class PostfixTokenizer {

	private String expression;
	private int pos;

	public PostfixTokenizer(String expression) {
		this.expression = expression;
	}

	public String nextToken() {
		String numberToken = nextNumber();
		if (numberToken != null)
			return numberToken;
		skipCommas();
		return nextOperator();
	}

	private String nextOperator() {
		if (pos == expression.length()) {
			return null;
		}
		return String.valueOf(expression.charAt(pos++));
	}

	private String nextNumber() {
		StringBuilder token = new StringBuilder();
		while (pos < expression.length() && isDigit(expression.charAt(pos))) {
			token.append(expression.charAt(pos++));
		}
		if (token.length() == 0)
			return null;
		return token.toString();
	}

	private void skipCommas() {
		while (pos < expression.length() && ',' == expression.charAt(pos))
			pos++;
	}

}